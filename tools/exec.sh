#! /bin/bash

red="\e[91m"
yellow="\e[93m"
default="\e[39m"

if [ -z "$1" ]; then
    echo -e "${red}No build specified.${default}"
    echo -e "command syntax : ./exec.sh <build> <world> <args>"
    echo -e "( i.e. ./exec.sh cmake-build-debug new_world \"-n 1000\")"
    exit
fi

build=$1

if [ -z "$2" ]; then
    echo -e "${red}No world name specified.${default}"
    echo -e "command syntax : ./exec.sh <build> <world> <args>"
    echo -e "( i.e. ./exec.sh cmake-build-debug new_world \"-n 1000\")"
    exit
fi

world=$2
args=$3

cd ..

if [ ! -d "$build" ]; then
    echo -e "${red}There is no build named $build.${default}"
    echo -e "Please make sure you have correclty built micro_aevol."
    echo -e "Aborting script."
    exit
fi

if [ ! -d "experiments" ]; then
    mkdir experiments
fi

cd ./experiments

if [ -d $world ]; then
    echo -e "${red}World $world already exist !${default}"
    echo -e "Please remove it manually to avoid making mistakes..."
    echo -e "Aborting script."
    exit
fi

mkdir $world
cd ./$world

echo -e "Executing command : ${yellow}./../../$build/micro_aevol_cpu ${args}${default}"

./../../$build/micro_aevol_cpu $args
