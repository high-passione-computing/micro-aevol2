#! /bin/bash

# color codes for kawaii tty prints. uwu
red="\e[91m"
green="\e[92m"
default="\e[39m"

ref_world=reference
test_world=

if [ -z "$1" ]; then
    echo -e "${red}No world name given.${default}"
    echo "Script syntax : ./checkdiff.sh <worldname> [other-world-name]"
    echo "Aborting script."
    exit
fi

test_world="$1"

if [ ! -z "$2" ]; then
    echo "Second paramater given. Overriding reference comparison."
    ref_world="$2"
fi

echo "Comparing $test_world with $ref_world."

cd ../experiments || echo "Aborting script." || exit

if [ ! -d "$ref_world" ]; then
    echo -e "${red}No world $ref_world has been found in experiments directory.${default}"
    echo "Aborting script."
    exit
fi

if [ ! -d "$test_world" ]; then
    echo -e "${red}No world $test_world has been found in experiments directory.${default}"
    echo "Aborting script."
    exit
fi

echo -n "Differences between backup files : "
backup_diff=`diff $ref_world/backup $test_world/backup`
if [ -z "$backup_diff" ]; then
    echo -e "${green}No differences found !${default}\n"
else
    echo -e "${red}Differences found.{$default}"
    echo "$backup_diff\n"
fi

echo -n "Differences between stats files : "
stats_diff=`diff $ref_world/stats $test_world/stats`
if [ -z "$stats_diff" ]; then
    echo -e "${green}No differences found !${default}\n"
else
    echo -e "${red}Differences found.{$default}"
    echo "$stats_diff\n"
fi

