#! /bin/bash

red="\e[91m"
green="\e[92m"
yellow="\e[93m"
default="\e[39m"

branch=$(git symbolic-ref -q HEAD)
branch=${branch##refs/heads/}
branch=${branch:-HEAD}

git checkout base
cd ..

if [ -d build-base ]; then
    echo -e "build-base already exist."
    echo -e "Removing & recompiling build base."
    rm -rf build-base
fi

mkdir build-base
cd ./build-base

cmake ..
make

cd ..

if [ ! -d experiments ]; then
    echo -e "experiments folder missing."
    echo -e "Creating folder."
    mkdir experiments
fi

cd ./experiments

mkdir reference
cd ./reference

if [ -d reference ]; then
    echo -e "${red}There is already a world named reference.${default}"
    echo -e "Please remove it manually to avoid making mistakes."
    echo -e "Aborting script."
    exit
fi

mkdir reference
echo -e "Running command : ${yellow} ../../build-base/micro_aevol_cpu -n 10000 ${default}"
../../build-base/micro_aevol_cpu -n 10000


git checkout $branch
